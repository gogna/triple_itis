package triple_itis

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const itisURL = "https://www.itis.gov/downloads/itisMySQLTables.tar.gz"

func Download() string {
	tokens := strings.Split(itisURL, "/")
	fileName := tokens[len(tokens)-1]
	path := filepath.Join(os.TempDir(), fileName)
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		fmt.Printf("The %s file already exists, skipping download\n", path)
		return path
	}
	fmt.Println("Downloading ITIS MySQL dump")

	output, err := os.Create(path)
	if err != nil {
		fmt.Println("Error while creating", path, "-", err)
		return path
	}
	defer output.Close()

	response, err := http.Get(itisURL)
	if err != nil {
		fmt.Println("Error while downloading", itisURL, "-", err)
		return path
	}
	defer response.Body.Close()

	_, err = io.Copy(output, response.Body)
	if err != nil {
		fmt.Println("Error while downloading", itisURL, "-", err)
		return path
	}
	return path
}

func UnzipItis(path string) {
	tarFile := filepath.Join(os.TempDir(), "itis.tar")
	err := ungzip(path, tarFile)
	if err != nil {
		fmt.Println(err)
	}

	paths, err := untar(tarFile, os.TempDir())
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Unzipped files:")
	for _, v := range paths {
		fmt.Println(v)
	}
}
func ungzip(source, target string) error {
	reader, err := os.Open(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	archive, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	defer archive.Close()

	target = filepath.Join(target, archive.Name)
	writer, err := os.Create(target)
	if err != nil {
		return err
	}
	defer writer.Close()

	_, err = io.Copy(writer, archive)
	return err
}

func untar(tarball, target string) ([]string, error) {
	var paths []string
	reader, err := os.Open(tarball)
	if err != nil {
		return paths, err
	}
	defer reader.Close()
	tarReader := tar.NewReader(reader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return paths, err
		}

		path := filepath.Join(target, header.Name)
		info := header.FileInfo()
		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); err != nil {
				return paths, err
			}
			continue
		}
		paths = append(paths, path)
		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if err != nil {
			return paths, err
		}
		defer file.Close()
		_, err = io.Copy(file, tarReader)
		if err != nil {
			return paths, err
		}
	}
	return paths, nil
}
