PROJECT = triple_itis
GOCMD = go
GOBUILD = $(GOCMD) build
GOINSTALL = $(GOCMD) install
GOCLEAN = $(GOCMD) clean
GOGET = $(GOCMD) get
GOTEST = $(GOCMD) test ./...
FLAG_MODULE = GO111MODULE=on
FLAGS_SHARED = $(FLAG_MODULE) CGO_ENABLED=0 GOARCH=amd64
FLAGS_LINUX = $(FLAGS_SHARED) GOOS=linux
FLAGS_MAC = $(FLAGS_SHARED) GOOS=darwin
FLAGS_WIN = $(FLAGS_SHARED) GOOS=windows

VERSION = $(shell git describe --tags)
VER = $(shell git describe --tags --abbrev=0)
DATE = $(shell date -u '+%Y-%m-%d_%H:%M:%S%Z')

all: install

test: deps install
	$(FLAG_MODULE) $(GOTEST)

test-build: deps build

deps:
	$(FLAG_MODULE) $(GOGET) github.com/spf13/cobra/cobra@67fc483; \
	$(FLAG_MODULE) $(GOGET) github.com/onsi/ginkgo/ginkgo@eea6ad0; \
	$(FLAG_MODULE) $(GOGET) github.com/onsi/gomega@abeb93d; \
  $(FLAG_MODULE) $(GOGET) golang.org/x/tools/cmd/goimports

version:
	echo "package triple_itis" > version.go
	echo "" >> version.go
	echo "const Version = \"$(VERSION)"\" >> version.go
	echo "const Build = \"$(DATE)\"" >> version.go

build: version
	cd $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_SHARED) $(GOBUILD)

install: version
	cd $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_SHARED) $(GOINSTALL)

release: version
	cd $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_LINUX) $(GOBUILD); \
	tar zcf /tmp/$(PROJECT)-$(VER)-linux.tar.gz $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_MAC) $(GOBUILD); \
	tar zcf /tmp/$(PROJECT)-$(VER)-mac.tar.gz $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_WIN) $(GOBUILD); \
	zip -9 /tmp/$(PROJECT)-$(VER)-win-64.zip $(PROJECT).exe; \
	$(GOCLEAN);