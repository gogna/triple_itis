module gitlab.com/gogna/triple_itis

go 1.12

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.1-0.20190417161816-abeb93df1e82
	github.com/spf13/cobra v0.0.4-0.20190321000552-67fc4837d267
	github.com/spf13/viper v1.3.2
	gitlab.com/gogna/gnparser v0.8.0
	golang.org/x/tools v0.0.0-20190425222832-ad9eeb80039a // indirect
)
