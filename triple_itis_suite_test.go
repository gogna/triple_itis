package triple_itis_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestTripleItis(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "TripleItis Suite")
}
